let socket = new WebSocket("ws://localhost:8080");
let image;
/**
 * Function to place image in the variable image
 */
let getFile = function(event) {
    let input = event.target;
    let reader = new FileReader();
    reader.onload = function () {
        image = reader.result;
    };
    if (!!input.files[0]) reader.readAsDataURL(input.files[0]);
};

/**
 * Sends message from form
 */
document.forms.publish.onsubmit = function() {
    let outgoingMessage = this.message.value;
    let file = this.fileimage.value;
    if (!!file) {
        socket.send (JSON.stringify ({
            type: 'messageImage',
            message: outgoingMessage,
            image: image
        }));
        return false;
        }
    socket.send (JSON.stringify ({
        type: 'message',
        message: outgoingMessage
    }));
    return false;
};

/**
 * incoming message handler
  */
socket.onmessage = function(event) {
    let incomingMessage = event.data;
    showMessage(incomingMessage);
};

/**
 * function for drawing message in div#messages
 */
function showMessage(messageFromServer) {
    let messageElem = document.createElement('div');
    let event = JSON.parse(messageFromServer);
    switch (event.type) {
        // if only message
        case 'message':
            messageElem.appendChild(document.createTextNode(event.message));
            document.getElementById('messages').appendChild(messageElem);
            break;
        // if message with image
        case 'messageImage':
            let textElem = document.createElement('div');
            textElem.appendChild(document.createTextNode(event.message));
            let imgElement = new Image(300);
            messageElem.appendChild(textElem);
            messageElem.appendChild(imgElement);
            imgElement.src = event.image;
            document.getElementById('messages').appendChild(messageElem);
            break;
        // if something wrong
        default:
            console.log('unknown event:', event);
            break;
    }
}
