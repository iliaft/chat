const express = require('express');
const app = express();
const port = 3000;
app.use(express.static('public'));

let counter = 0;
let memberID = 0;
let members = {};

let WebSocketServer = require('ws').Server;
let wss = new WebSocketServer({port: 8080});

wss.on('connection', function(ws) {
    counter++;
    memberID++;
    members[memberID] = ws;
    /**
     * First message broadcasting
     */
    for (let key in members) {
        let hiMessage = JSON.stringify ({
            type: 'message',
            message: 'now online ' + counter + 'people'
        });
        members[key].send(hiMessage);
    }

    ws.on('message', function(message) {
/*        console.log('message - ', message);*/
        for (let key in members) {
            members[key].send(message);
        }
    });

    ws.on('close', function() {
        counter--;
        delete members[memberID];
        for (let key in members) {
            let hiMessage = JSON.stringify ({
                type: 'message',
                message: 'now online ' + counter + 'people'
            });
          members[key].send(hiMessage);
        }
    });

    ws.on('error', function(error) {
        console.log("Error - " + error.message);
    });
});

app.get('/', (request, response) => {
    response.sendFile(path.join(__dirname + 'index.html'))
});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${port}`)
});
